import * as React from "react"
import { Router } from "@reach/router"
import CardController from "../components/Cards/card-contorller"
import Lesson from "./lessons/[id]"
import Layout from "../partials/layouts/layout"

const IndexPage = () => {
  return  (
      <Layout>
         <Router basepath="/">
          <Lesson path="/lessons:id" />
        </Router>
          <CardController/>
      </Layout>
    )
}

export default IndexPage
