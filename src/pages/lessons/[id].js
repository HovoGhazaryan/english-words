import React, {useEffect, useState} from "react";
import { Link, useLocation } from '@reach/router';
import Layout from "../../partials/layouts/layout";
import Words from "../../components/Words/words";
import { wordsData } from "../../components/Data";
const Lesson = () => {
    const [activeLesson, setActiveLesson] = useState([{ word: 'Honest', translate: 'Ազնիվ', id: 1, count: 0, lesson: 1 },])
    const { href } = useLocation()
    useEffect(() => {
      setActiveLesson([...wordsData.filter(item => +item.lesson === +href?.split('/')[4])])
    }, [ href ])
    return (<Layout>
                { activeLesson.length ? <Words wordsData={activeLesson} setWordData={setActiveLesson}/> 
                : <div className="question">
                    <h1 style={{color: 'white'}}>Congratulations <Link to="/">Go To The Home{`->`}</Link></h1>
                  </div>
                }
            </Layout>
        )
}

export default Lesson;