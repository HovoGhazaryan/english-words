import * as React from "react"
import { Helmet } from "react-helmet"
import "../layout.css"

const Layout = ({ children }) => {
  return (
    <>
        <Helmet>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"/>
        </Helmet>
      <div
        style={{
          margin: `0 auto`,
          padding: '30px',
          width: '100%',
          height: '100%',
          position: 'absolute',
          background: 'rgb(11 40 47 / 94%)'
        }} >        
        {children}
      </div>
    </>
  )
}


export default Layout
