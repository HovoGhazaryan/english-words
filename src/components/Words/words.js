import { Link } from "gatsby"
import React, { useEffect, useState } from "react"

const Words = ({ wordsData, setWordData }) => {
    const [elements, setElements] = useState([])
    useEffect(() => {
        setElements([...wordsData])
    }, [wordsData])
    const controller = {
        currentWord: null,
        questionFn: () => {
            if(wordsData.length) {
                const random = Math.floor(Math.random() * wordsData.length);
                if( wordsData[random] && wordsData[random].count < 3 ) {
                    controller.currentWord = wordsData[random]
                    return wordsData[random].word;
                } else {
                    return controller.questionFn()
                }
            } else {
                return <h1>Congratulations</h1>
            }
        },
        answerFn: answer => {
            const question = wordsData.find(item => item === controller.currentWord)
            const index = wordsData.findIndex(item => item === question)
            if(answer === question.word || answer === question.translate) {
                if(question.count < 3) {
                    setWordData([...wordsData.slice(0, index), 
                        {...question, count: question.count += 1},
                        ...wordsData.slice(index + 1 )])
                    if(question.count === 3) {
                        setWordData([...wordsData.slice(0, index), 
                            ...wordsData.slice(index + 1 )])
                    }
                }
            } else {
                alert('Ooh come on')
            }
        },
    }
    return (
    <div className="word" style={{width: '100%', height: '100%',}}>
        <div style={{color: 'white', fontSize: '20px'}}><Link to="/" color="white">Home</Link></div>
        <div className="question">
            <strong>{controller.questionFn()}</strong>
            <span className="count">{controller.currentWord?.count}</span>
        </div>
        <div className="collection answers">
            { elements.map( item => {
            const {translate, id} = item;
            return <button onClick={() => controller.answerFn(translate)} 
                           key={`${id}`}
                           className="answer__button waves-effect waves-light btn">
                            {translate}
                    </button>
        }) }
        </div>
    </div>)
}

export default Words