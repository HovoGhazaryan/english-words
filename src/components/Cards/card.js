import React from "react";
import { Link } from "gatsby";
import './index.css'
const Card = ({data: {lesson, description, lessonURL}}) => {
    return (
      <div className="col s4 m4">
        <Link to={`${lessonURL}`}>
          <div className="card lesson_grid">
            <div className="card-content white-text">
              <span className="card-title">{ lesson }</span>
              <p>{ description }</p>
            </div>
            <div className="card--action">
              <p>Go to the lesson</p>
            </div>
          </div>
        </Link>
      </div>
    )

}

export default Card