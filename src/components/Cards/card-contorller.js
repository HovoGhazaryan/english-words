import React from "react";
import Card from "./card";

const CardController = () => {
    const lessions = [
        {lesson: 'Lesson 1', description: '', lessonURL: '/lessons/1'},
        {lesson: 'Lesson 2', description: '', lessonURL: '/lessons/2'},
        {lesson: 'Lesson 3', description: '', lessonURL: '/lessons/3'},
        {lesson: 'Lesson 4', description: '', lessonURL: '/lessons/4'},
        {lesson: 'Lesson 5', description: '', lessonURL: '/lessons/5'},
        {lesson: 'Lesson 6', description: '', lessonURL: '/lessons/6'},
    ]
    return (
        <div className="row">
            {lessions.map((item) => {
                return <Card key={item.lesson} data={item}/> 
            })}
        </div>
    )
}

export default CardController